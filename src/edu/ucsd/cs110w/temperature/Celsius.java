package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature {
	public Celsius(float t) {
		super(t);
	}

	public String toString() {
		return Float.toString(value) + " C";
	}

	@Override
	public Temperature toCelsius() {
		return new Celsius(value);
	}

	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit((float) ((9.0 / 5.0) * value + 32));
	}

	@Override
	public Temperature toKelvin() {
		return new Kelvin((float) (value-273.15));
	}
}