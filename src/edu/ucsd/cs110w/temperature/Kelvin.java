package edu.ucsd.cs110w.temperature;

public class Kelvin extends Temperature {
	public Kelvin(float t) {
		super(t);
	}

	public String toString() {
		return Float.toString(value) + " K";
	}

	@Override
	public Temperature toCelsius() {
		return new Celsius((float) (value - 273.15));
	}

	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit((float) ((9.0 / 5.0) * (value - 273.15) + 32));
	}

	@Override
	public Temperature toKelvin() {
		return new Kelvin(value);
	}
}