package edu.ucsd.cs110w.temperature;

public class Fahrenheit extends Temperature {
	public Fahrenheit(float t) {
		super(t);
	}

	public String toString() {
		return Float.toString(value) + " F";
	}

	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius((float) ((5.0 / 9.0) * (value - 32)));

	}

	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit(value);

	}

	@Override
	public Temperature toKelvin() {
		return new Kelvin((float) ((float) ((5.0 / 9.0) * (value - 32))-273.15));
	}
}
